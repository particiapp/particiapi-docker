in_comment && /^#/ {
    sub(/^# */, "", $0)
    comment = comment "\n" $0
    next
}

/^#/ {
    sub(/^# */, "", $0)
    comment = $0
    in_comment = 1
    next
}

/^[A-Z_]+=/ && comment != "" {
    n = index($0, "=")
    name = substr($0, 1, n - 1)
    value = substr($0, n + 1)
    printf("### %s\n\n", name)
    printf("%s\n\n", comment)
    if (value != "") {
        printf("**default**: %s\n\n", value)
    }
}

{
    comment = ""
    in_comment = 0
}
