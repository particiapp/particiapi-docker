#!/bin/sh

mkdir -p /certs/certs || exit 1
mkdir -p -m 700 /certs/private || exit 1
exec "$@"
