#!/bin/sh

usage () {
    printf 'usage: %s [-k key_file] [-c cert_file] name[,name ...]\n' \
        "$(basename $0)" >&2
}

key_file=key.pem
cert_file=certificate.pem
subjectaltnames=
cn=

while getopts "c:hk:" opt; do
    case $opt in
    c)
        cert_file="${OPTARG}"
        ;;
    h)
        usage
        exit 0
        ;;
    k)
        key_file="${OPTARG}"
        ;;
    *)
        usage
        exit 2
        ;;
    esac
done
shift "$((OPTIND-1))"

if [ $# -eq 0 ]; then
    printf 'no dns names specified\n' >&2
    exit 1
fi

cn="$1"

if [ -e "${cert_file}" ] || [ -e "${key_file}" ]; then
    printf 'key/certificate file already exists\n' >&2
    exit 0
fi

for name; do
    subjectaltnames="${subjectaltnames}${subjectaltnames:+, }DNS:${name}"
done

openssl req \
    -batch \
    -newkey rsa:4096 \
    -nodes \
    -keyout "${key_file}" \
    -x509 \
    -sha256 \
    -days 365 \
    -subj "/CN=${cn}" \
    -out "${cert_file}" \
    -addext "subjectAltName=${subjectaltnames}"
