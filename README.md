# Docker Compose Environment for Particiapp/Polis

This Docker Compose environment provides an easy way to [deploy][1] Polis
with the Particiapi server.  It includes all Polis services as well as a
PostgreSQL database but no frontend or OpenID Connect identity provider.

For evaluation and development a [development environment][2] with a
preconfigured example setup is provided.  It adds additional services including
an example frontend, an OpenID Connect identity provider, a database frontend,
and a mail server.


[1]: ./doc/production-environment.md
[2]: ./doc/development-environment.md
