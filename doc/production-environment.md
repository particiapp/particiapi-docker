# Production environment

The Docker Compose production environment provides an easy way to deploy Polis with the Particiapi server.
It includes a Particiapi server, all Polis services as well as a PostgreSQL database but no frontend, OpenID Connect identity provider or email sending service.

## Setup

Check out the Particiapp repositories into a common base directory:

```
git clone https://gitlab.com/particiapp/particiapi.git
git clone https://gitlab.com/particiapp/particiapp-docker.git
```

## Configuration

The Polis and Particiapi containers need to be configured before they can be started.
Inside the particiapp-docker repository copy `env` to `.env` and ensure all mandatory configuration settings mentioned below are set.

### ROOT_DIR

Specify the directory containing the particiapi-docker project.

### DATA_DIR

Specify the base directory where persistent data is stored.

### PARTICIAPI_DIR

Specify the directory containing the particiapi project.

### PARTICIAPP_FRONTEND_DIR

Specify the directory containing the particiapp-example-frontend project.

### POSTGRES_PASSWORD

Set the password of the PostgreSQL user.

### POLIS_SERVER_NAME

Set the fully qualified name of the Polis server.

### PARTICIAPI_HOSTNAME

Set the hostname of the Particiapi server.

### PARTICIAPI_DOMAINNAME

Set the domainname of the Particiapi server.

### PARTICIAPI_SECRET_KEY

Set the Particiapi secret key used for signing cookies and similar needs.

### PARTICIAPI_CORS_ORIGINS

Specify the API frontend origin for CORS.

### PARTICIAPI_IDP_API_BASE_URL

Set the OpenID Connect identity provider base URL.

### PARTICIAPI_IDP_CLIENT_ID

Set the OpenID Connect identity provider client ID.

### PARTICIAPI_IDP_CLIENT_SECRET

Set the OpenID Connect identity provider secret.

### PARTICIAPI_AUTHENTICATION_DISABLED

Set to true in order to allow participants to vote and submit statements
without authentication (not recommended for publicly accessible instances).

### BIND_ADDRESS

Specify the address to listen on.

**default**: 127.0.0.1

### UWSGI_PROCESSES

Set the number of uwsgi processes (see
https://uwsgi-docs.readthedocs.io/en/latest/).

**default**: 4

### UWSGI_THREADS

Set the number of uwsgi threads per process (see
https://uwsgi-docs.readthedocs.io/en/latest/).

**default**: 4

### POLIS_FROM_ADDRESS

Set the email from address used in emails sent by Polis.

**default**: "Polis <polis@${POLIS_SERVER_NAME}>"

### EMAIL_TRANSPORT_TYPES

Specify a list of email transports which will be tries in order until sending
succeeds (supported values: aws-ses, mailgun, maildev).

### AWS_ACCESS_KEY_ID

Specify the AWS SES key ID if the "aws-ses" email transport is used.

### AWS_SECRET_ACCESS_KEY

Specify the AWS SES API key if the "aws-ses" email transport is used.

### MAILGUN_DOMAIN

Specify the Mailgun domain if the "mailgun" email transport is used.

### MAILGUN_API_KEY

Specify the Mailgun API key if the "mailgun" email transport is used.

## Running the container environment

Run the following command to build and run the containers:

    cd particiapp-docker && docker compose up

The Particiapi HTTP API server listens on the configured address on port 8000
and the Polis server on port 8001.


!!! warning Exposing the Polis server

    Note that when you make the Polis server publicly available anyone can
    participate in conversations without authentication, register an account
    and create new conversations.
