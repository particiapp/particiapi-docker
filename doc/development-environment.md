# Development environment

For evaluation and development a development environment with a preconfigured example setup is provided.
It includes preconfigured instances of Particiapi server, all Polis services, a PostgreSQL database, Keycloak (OpenID Connect identity provider), a pgAdmin (database frontend), and Maildev (SMTP test server).

## Setup

Check out the Particiapp repositories as well as the example client into a common base directory:

```
git clone https://gitlab.com/particiapp/particiapi.git
git clone https://gitlab.com/particiapp/particiapp-example-frontend.git
git clone https://gitlab.com/particiapp/particiapp-docker.git
```

## Running the container environment

Run the following command to build and run the containers:

```
cd particiapp-docker && docker compose --env-file dev.env -f docker-compose.yaml -f docker-compose.dev.yaml up
```

## Services

The following services are made available:

| Service | URL | Comment |
|---------|-----|---------|
| Polis frontend | <https://polis.example.org/> | Sign up for a new user account in order to add conversations |
| Particiapi server | <https://particiapi.example.org/> | |
| Particiapp reference frontend | [https://www.example.com/?&shy;base_url=&shy;https%3A%2F%2Fparticiapi.example.org%2F&&shy;conversation_id=XXXXXXXXXX](https://www.example.com/base_url=https%3A%2F%2Fparticiapi.example.org%2F&conversation_id=XXXXXXXXXX) | Replace "XXXXXXXXX" with the conversation identifier from the Polis admin frontend |
| Keycloak | <https://idp.example.net> | <ul><li>Administrator<ul><li>Username: admin</li><li>Password: admin</li></ul></li><li>User<ul><li>Username: test</li><li>Password: test</li></ul></li></ul> |
| pgAdmin | <https://pgadmin.example.net> | <ul><li>Username: admin@pgadmin.example.net</li><li>Password: admin</li></ul> |
| Maildev | <https://maildev.example.net> | |

## Configuring your web browser

The development environment simulates a deployment scenario where the Polis and Particiapp frontends, the Particiapi server (and all other services) are served from different domains via HTTPS in order to allow for testing CORS.
DNS resolution is provided accordingly and no services except a SOCKS5 proxy are directly exposed to the host.
Web browsers and other HTTP clients must thus be configured to use the SOCKS5 proxy listening on port 1080 in order to access any of the services attached to the Docker network and also use this proxy for DNS resolution.
When the Docker compose environment is started for the first time, self-signed certificates will be generated for all of the above services.  Make sure to visit all of them and add exceptions for them in your browser.

### Chrome

The Chrome web browser must be started with the following command line options in order to use the poxy listening on localhost: `--proxy-server="socks5://127.0.0.1:1080" --host-resolver-rules='MAP * ~NOTFOUND, EXCLUDE 127.0.0.1'`

When visiting any of the above URLs for the first time you will need to add an exception for the self-signed certificates as follows:

![Chrome security exception](chrome-security-exception.png){ width="980" }

### Firefox

In Firefox open "Settings" from the menu, in the "General" panel go to the "Network Settings" section and click on the "Settings…" button.

![Firefox general settings](firefox-general-settings.png){ width="980" }

Select "Manual proxy configuration" and fill in "127.0.0.1" as the "SOCKS Host" and "1080" as the "Port", select "SOCKS v5" and "Proxy DNS when using SOCKS v5" below.

![Firefox network settings](firefox-network-settings.png){ width="980" }

When visiting any of the above URLs for the first time you will need to add an exception for the self-signed certificates as follows:

![Firefox security exception](firefox-security-exception.png){ width="980" }

## Development

Any changes to the particiapi and particiapp-example-frontend repositories are mounted into their respective containers and the Particiapi server will be automatically reloaded when changes are detected.
