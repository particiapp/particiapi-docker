# Architecture

## Production environment

A typical production environment consists of a Polis instance with its components (file, API, and statistics processing servers), a PostgreSQL database, and the Particiapi server which provides a cross-domain HTTP API for custom frontends.
In addition an OpenID Connect-based identity provider is required for authenticating users and an email sending service for sending out notifications.  The actual frontend can be served by any third party which has been granted access to the API via CORS.

``` mermaid
architecture-beta
    service browser[Browser]

    group extidp[External service provider]
    service idp(cloud)[Identity provider] in extidp

    group extmail[Mailgun or Amazon SES]
    service mail(cloud)[Email service] in extmail

    group extfrontend[3rd party]
    service frontend(cloud)[Particiapp frontend] in extfrontend

    group particiapp[Particiapp]
    service particiapi(server)[API server] in particiapp

    group polis[Polis] in particiapp
    service postgres(database)[Database] in polis
    service polisserver(server)[API server] in polis
    service polismath(server)[Statistics processing] in polis
    service polisfileserver(disk)[File server] in polis

    junction j01
    junction j02
    junction j03
    junction j04
    junction j05
    junction j11

    browser:B -- T:j03
    j01:R -- L:j02
    j02:R -- L:j03
    j03:R -- L:j04
    j04:R -- L:j05
    j01:B -- T:j11
    j11:B -- T:frontend
    j02:B -- T:idp
    idp:R -- L:particiapi
    j03:B -- T:particiapi
    particiapi:R -- L:postgres
    postgres:R -- L:polisserver
    j05:B -- T:polisserver
    postgres:B -- T:polismath
    polisserver:B -- T:polisfileserver
    polisserver:R -- L:mail
```

## Development environment

Apart from the Polis instance and Particiapi server the development environment already includes a preconfigured OpenID Connect identity provider ([Keycloak](https://www.keycloak.org/)), test email server ([Maildev](https://maildev.github.io/maildev/)) and a database frontend ([pgAdmin](https://www.pgadmin.org/)).


``` mermaid
architecture-beta
    service browser[Browser]
    service proxy(internet)[Proxy]

    group dev[Development environment]
    service idp(internet)[Identity provider] in dev
    service mail(internet)[Email service] in dev
    service frontend(internet)[Particiapp frontend] in dev
    service pgadmin(internet)[Database frontend] in dev

    group particiapp[Particiapp] in dev
    service particiapi(server)[API server] in particiapp

    group polis[Polis] in particiapp
    service postgres(database)[Database] in polis
    service polisserver(server)[API server] in polis
    service polismath(server)[Statistics processing] in polis
    service polisfileserver(disk)[File server] in polis

    junction j01 in dev
    junction j02 in dev
    junction j03 in dev
    junction j04 in dev
    junction j05 in dev
    junction j06 in dev
    junction j11 in dev
    junction j12 in dev
    junction j13 in dev
    junction j14 in dev
    junction j15 in dev

    browser:B -- T:proxy
    proxy:B -- T:j03
    j01:B -- T:j11
    j01:R -- L:j02
    j02:B -- T:j12
    j02:R -- L:j03
    j03:B -- T:j13
    j03:R -- L:j04
    j04:B -- T:pgadmin
    pgadmin:B -- T:postgres
    j04:R -- L:j05
    j05:B -- T:j15
    j05:R -- L:j06
    j06:B -- T:mail
    j11:B -- T:frontend
    j12:B -- T:idp
    idp:R -- L:particiapi
    j13:B -- T:particiapi
    particiapi:R -- L:postgres
    postgres:R -- L:polisserver
    j15:B -- T:polisserver
    postgres:B -- T:polismath
    polisserver:B -- T:polisfileserver
    polisserver:R -- L:mail
```
